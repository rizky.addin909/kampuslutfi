-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 10, 2020 at 07:24 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kampus`
--

-- --------------------------------------------------------

--
-- Table structure for table `beritas`
--

CREATE TABLE `beritas` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `berita` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `beritas`
--

INSERT INTO `beritas` (`id`, `user_id`, `berita`, `created_at`, `updated_at`) VALUES
(6, 11, 'korona semakin merajalela banyak pasien yang bertambah', '2020-08-08 19:37:57', '2020-08-08 19:37:57'),
(8, 11, 'ini berita apaya', '2020-08-09 07:09:26', '2020-08-09 07:09:26');

-- --------------------------------------------------------

--
-- Table structure for table `siswas`
--

CREATE TABLE `siswas` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `nama_depan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_belakang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telepon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `siswas`
--

INSERT INTO `siswas` (`id`, `user_id`, `nama_depan`, `nama_belakang`, `email`, `password`, `telepon`, `alamat`, `status`, `foto`, `created_at`, `updated_at`) VALUES
(9, 13, 'riztunah', 'local', 'admin@ayosinau.com', NULL, '087711870215', 'jl.sukun', 'Lulus', NULL, '2020-07-17 11:45:20', '2020-08-09 20:29:04'),
(10, 14, 'kotobuki', NULL, 'amikom@gmail.com', NULL, NULL, NULL, NULL, NULL, '2020-07-18 08:01:54', '2020-07-18 08:01:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(244) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `provider`, `provider_id`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(11, 'Rizky Addin', 'muhammad.1050@students.amikom.ac.id', '', NULL, '$2y$10$8bw0z.sYkljqWV6m9gk.VuDXiJIFf6kgZ.2OMiBoGC.FxXFufY0fO', NULL, NULL, 'admin', NULL, '2020-07-15 16:52:41', '2020-07-15 16:52:41'),
(12, 'akihito Hibari', 'mio.akiyama909@gmail.com', '', NULL, '$2y$10$Z57S7JrDBLumOkMw8anDWuH5wKYUtqIjkFUX4nNKpGp7gCr/ku81K', NULL, NULL, 'siswa', NULL, '2020-07-16 23:10:44', '2020-07-17 17:45:02'),
(13, 'riztunah', 'admin@ayosinau.com', '1595054538.png', NULL, '$2y$10$shkM3YsYjITMBhAQesmwS.Hxw9/zeQZw.m82wfNjQ9NdpkIDO30xK', NULL, NULL, 'siswa', NULL, '2020-07-17 08:42:46', '2020-07-18 07:57:46'),
(14, 'kotobuki', 'amikom@gmail.com', '1595084677.png', NULL, '$2y$10$FMHZT/3jJ4lBAx2G7v7w/OW0ZkJYD5qgtCTW.8e7ww1qJVUi6HZu.', NULL, NULL, 'siswa', NULL, '2020-07-18 08:01:47', '2020-07-18 08:04:37');

-- --------------------------------------------------------

--
-- Table structure for table `wisudas`
--

CREATE TABLE `wisudas` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_siswa` int(10) UNSIGNED DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nama_lengkap` varchar(255) DEFAULT NULL,
  `nim` varchar(255) DEFAULT NULL,
  `alamat_email` varchar(255) DEFAULT NULL,
  `nohp` varchar(255) DEFAULT NULL,
  `nama_ortu` varchar(255) DEFAULT NULL,
  `prodi` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` varchar(255) DEFAULT NULL,
  `alamat_domisili` varchar(255) DEFAULT NULL,
  `nik` varchar(255) DEFAULT NULL,
  `file_kk` varchar(255) DEFAULT NULL,
  `file_akte` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wisudas`
--

INSERT INTO `wisudas` (`id`, `id_siswa`, `email`, `nama_lengkap`, `nim`, `alamat_email`, `nohp`, `nama_ortu`, `prodi`, `tempat_lahir`, `tanggal_lahir`, `alamat_domisili`, `nik`, `file_kk`, `file_akte`, `created_at`, `updated_at`) VALUES
(47, 9, 'admin@ayosinau.com', 'addin saputra', '17.11.1050', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1597030067.pdf', '1597030079.pdf', '2020-08-09 19:10:10', '2020-08-09 20:27:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beritas`
--
ALTER TABLE `beritas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `siswas`
--
ALTER TABLE `siswas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wisudas`
--
ALTER TABLE `wisudas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_siswa` (`id_siswa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beritas`
--
ALTER TABLE `beritas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `siswas`
--
ALTER TABLE `siswas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `wisudas`
--
ALTER TABLE `wisudas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `beritas`
--
ALTER TABLE `beritas`
  ADD CONSTRAINT `beritas_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `siswas`
--
ALTER TABLE `siswas`
  ADD CONSTRAINT `siswas_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `wisudas`
--
ALTER TABLE `wisudas`
  ADD CONSTRAINT `wisudas_ibfk_1` FOREIGN KEY (`id_siswa`) REFERENCES `siswas` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
