@extends('../layout/applog')

@section('title','Nama Website')


<!-- Page content -->
@section('content')
  <!-- Header -->
  <header class="w3-container" style="padding-top:22px">
    <h4><span>Data <strong>Yudisium</strong></span><br><h4>
  </header>

  <div class="w3-panel">
    <div class="w3-row-padding" style="margin:0 -16px">
      <div class="w3-twothird">
        <h5>Feeds</h5>
        <table class="w3-table w3-striped w3-white">
          @foreach( $wisuda as $wisud)
          @if($wisud->id_siswa == $id)
              <form action="{{route('editwisudaupdate')}}" method="post">
                @csrf
                <tr>
                  <td>
                    <p>Email : </p>
                    {{ $wisud->email }}
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Nama Lengkap : </p>
                    {{ $wisud->nama_lengkap }}
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Nim : </p>
                    {{ $wisud->nim }}
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Alamat Email : </p>
                    {{ $wisud->alamat_email }}
                  </td>
                </tr>

                <td>
                    <p>No Handphone (Masih Aktif) : </p>
                    {{ $wisud->nohp }}
                  </td>
                </tr>

                <td>
                    <p>Nama Orang Tua : </p>
                    {{ $wisud->nama_ortu }}
                  </td>
                </tr>

                <td>
                    <p>Program Studi : </p>
                    {{ $wisud->prodi }}
                  </td>
                </tr>

                <td>
                    <p>Tempat Lahir (Sesuai Kartu Keluarga) : </p>
                    {{ $wisud->tempat_lahir }}
                  </td>
                </tr>

                <td>
                    <p>Tanggal Lahir : </p>
                    {{ $wisud->tanggal_lahir }}
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Alamat Domisili (Asal) : </p>
                    {{ $wisud->alamat_domisili }}
                  </td>
                </tr>

                <tr>
                <td>
                    <p>NIK (Nomor Induk Kependudukan) : </p>
                    {{ $wisud->nik }}
                  </td>
                </tr>
                
                <input class="uk-input uk-width-1-1 uk-margin-large-left" type="hidden" name="sisid" value="{{ $wisud->id_siswa }}">

                <tr>
                <td>
                    <p>Upload Scan Asli Kartu Keluaga (file PDF) : </p>
                   <p> <iframe src=" {{url('uploads/wisudas/'.$wisud->file_kk)}}" height="500px" width="500px"></iframe></p> 
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Asli Akte Kelahiran (File PDF) : </p>
                    <p> <iframe src=" {{url('uploads/wisudas/'.$wisud->file_akte)}}" height="500px" width="500px"></iframe></p> 
                  </td>
                </tr>
                @foreach( $siswas as $siswa)
                @if($siswa->id == $id)
                <tr>
                  <td>
                    <p>Status Mahasiswa : </p>
                    <input type="text" name="stat" id="stat" value="{{$siswa->status}}">
                  </td>
                </tr>
                @endif
                @endforeach

                <tr>
                  <td>
                    <input type="submit" value="Kirim">
                  </td>
                </tr>

              </form>
          @endif
          @endforeach
          
        </table>
      </div>
    </div>
  </div>
  @endsection

  @section('footer')
  <!-- Footer -->
  <footer class="w3-container w3-padding-16 w3-light-grey">
    <center><p>Copyright © 1994 - 2020 LPP YOGYAKARTA. </p></center>
  </footer>
@endsection
