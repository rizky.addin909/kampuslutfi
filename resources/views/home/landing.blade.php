@extends('../layout/app')

@section('title','Nama Website')


<!-- Page content -->
@section('content')
    <div class="login-section">
          <div class="login-w3l"> 
              <h2 class="sub-head-w3-agileits">Information</h2> 
              <hr>   
              <div class="profile-info-w3layouts">
              <tr>
              @foreach($berita as $ber)
                <td><h6 style="color: white;">- {{ $ber->berita }}</h6></td>
                <br>
              @endforeach        
            </tr>
            </div>
      <!-- //login -->
          <div class="clear"></div>
          <hr>
          <div class="login-form"> 
          <p>Have an account?<a class="book popup-with-zoom-anim button-isi zoomIn animated" data-wow-delay=".5s" href="#small-dialog2"> Sign in</a> <br> OR <br> Don't have an account?<a class="book popup-with-zoom-anim button-isi zoomIn animated" data-wow-delay=".5s" href="#small-dialog"> Sign up</a> </p>
            </div> 
        </div>

        <div class="clear"></div>
    </div> 

@endsection

@section('footer')
<!-- Footer -->
    <p class="footer" style="color: black;">Copyright © 1994 - 2020 LPP YOGYAKARTA.</p>
      <!--//login-->
      <div class="pop-up"> 
  <div id="small-dialog" class="mfp-hide book-form">
    <h3 class="sub-head-w3-agileits"> <img src="/image/background/poster-lpp.png" height="55px" width="250px"> </h3>
      <form action="{{ route('register') }}" method="post">
        @csrf
        <div class="col-md-6">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="Username" autofocus>

            @error('name')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
            @enderror
        </div>
        <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
        <div class="col-md-6">
                                <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <input id="status" class="uk-input uk-form-width-xlarge uk-margin-small-bottom" type="hidden" name="status" value="Mahasiswa">
        <div class="col-md-6">
                                <input id="password-confirm" placeholder="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>          
        <input type="submit" value="Sign Up">
      </form>
  </div>
</div>


<div class="pop-up"> 
  <div id="small-dialog2" class="mfp-hide book-form">
<br>
    <h3 class="sub-head-w3-agileits"><img src="/image/background/poster-lpp.png" height="55px" width="250px"></h3>
      <form action="{{ route('login') }}" method="post">
        @csrf
        <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                <div class="signin-rit">
                  <span class="checkbox1">
                    <label class="checkbox" style="color: black;"><input type="checkbox" name="checkbox" checked="">Remember me</label>
                  </span>
                  <a class="forgot" href="#" style="color: black;">Forgot Password?</a>
                <div class="clear"> </div>
                </div>
                <input type="submit" value="Sign in">
      </form>
  </div>
</div>

@endsection
