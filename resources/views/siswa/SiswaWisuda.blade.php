@extends('../layout/applog')

@section('title','Nama Website')


<!-- Page content -->
@section('content')
@foreach( $users as $user)
@if($user->id == auth()->user()->id)
  <!-- Header -->
  <?php $ns = 0; ?>
  <header class="w3-container" style="padding-top:22px">
     <h4><span>Daftar <strong>Yudisium</strong></span><br><h4>
  </header>

  <div class="w3-panel">
    <div class="w3-row-padding" style="margin:0 -16px">
      <div class="w3-twothird">
        <h5>Daftar Wisuda</h5>
        <table class="w3-table w3-striped w3-white">
          @foreach( $wisuds as $wisu)
          @foreach( $siswa as $sis)
          @if($sis->user_id == auth()->user()->id)
          @if($wisu->id_siswa == $sis->id)
              <form action="{{ route('prosesdaftarwisuda') }}" method="post" enctype="multipart/form-data">
                @csrf
                @foreach( $siswa as $sis)
                @if($sis->user_id == auth()->user()->id)
                @if($sis->status =='Proses')
                <tr>
                  <td>
                    <p>Menunggu konfirmasi dari kampus </p>
                    @else
                    <p>Data Sudah di konfirmasi silahkan ke kampus untuk melanjutkan </p>
                  </td>
                </tr>
                @endif
                @endif
                @endforeach
                <tr>
                  <td>
                    <p>Email : </p>
                    <input class="uk-input uk-width-1-1 uk-margin-large-left" type="text" placeholder="Alamat Email" name="email" value="{{ $wisu->email }}">
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Nama Lengkap : </p>
                    <input class="uk-input uk-width-1-3 uk-margin-large-left" type="text" placeholder="Nama Depan" name="namlep" value="{{ $wisu->nama_lengkap }}" >
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Nim</p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="**.**.****" name="nim" value="{{ $wisu->nim }}" >
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Alamat Email</p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="ex(@gmail.com)" name="alemail" value="{{ $wisu->alamat_email }}">
                  </td>
                </tr>

                <tr>
                <td>
                    <p>No Handphone (Masih Aktif)</p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="08*******" name="nohp" value="{{ $wisu->nohp }}">
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Nama Orang Tua</p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="namortu" value="{{ $wisu->nama_ortu }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Program Studi</p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="prodi" value="{{ $wisu->prodi }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tempat Lahir (Sesuai Kartu Keluarga)</p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="temla" value="{{ $wisu->tempat_lahir }}">
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tanggal Lahir</p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="MM/DD/YYYY" name="tangla" value="{{ $wisu->tanggal_lahir }}" >
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Alamat Domisili (Asal) </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="alamat" value="{{ $wisu->alamat_domisili }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>NIK (Nomor Induk Kependudukan)</p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="nik" value="{{ $wisu->nik }}" >
                  </td>
                </tr>
                
                <input class="uk-input uk-width-1-1 uk-margin-large-left" type="hidden" name="sisid" value="{{ $wisu->id_siswa }}">


                <tr>
                <td>
                    <p>Tempat PKL I </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="tempat_pkl1" value="{{ $wisu->tempat_pkl1 }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tanggal Pelaksanaan I </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="tanggal_pelaksanaan" value="{{ $wisu->tanggal_pelaksanaan }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Judul PKL I </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="judul_pkl1" value="{{ $wisu->judul_pkl1 }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tempat PKL II </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="tempat_pkl2" value="{{ $wisu->tempat_pkl2 }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tanggal Pelaksanaan II </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="tanggal_pelaksanaan2" value="{{ $wisu->tanggal_pelaksanaan2 }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Judul PKL II </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="judul_pkl2" value="{{ $wisu->judul_pkl2 }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tempat PKL III </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="tempat_pkl3" value="{{ $wisu->tempat_pkl3 }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tanggal Pelaksanaan III </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="tanggal_pelaksanaan3" value="{{ $wisu->tanggal_pelaksanaan3 }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Judul PKL III </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="judul_pkl3" value="{{ $wisu->judul_pkl3 }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Judul Tugas Akhir </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="judul_tugas_akhir" value="{{ $wisu->judul_tugas_akhir }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Nama Pembimbing Tugas Akhir (1) </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="nama_pembimbing_tugas_akhir1" value="{{ $wisu->nama_pembimbing_tugas_akhir1 }}" >
                  </td>
                </tr>
                <tr>
                <td>
                    <p>Nama Pembimbing Tugas Akhir (2) </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="nama_pembimbing_tugas_akhir2" value="{{ $wisu->nama_pembimbing_tugas_akhir2 }}" >
                  </td>
                </tr>
                <tr>
                <td>
                    <p>Nama Pembimbing Tugas Akhir (3) </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="nama_pembimbing_tugas_akhir3" value="{{ $wisu->nama_pembimbing_tugas_akhir3 }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Nama Penguji Tugas Akhir (1) </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="nama_penguji_tugas_akhir1" value="{{ $wisu->nama_penguji_tugas_akhir1 }}" >
                  </td>
                </tr>
                <tr>
                <td>
                    <p>Nama Penguji Tugas Akhir (2) </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="nama_penguji_tugas_akhir2" value="{{ $wisu->nama_penguji_tugas_akhir2 }}" >
                  </td>
                </tr>
                <tr>
                <td>
                    <p>Nama Penguji Tugas Akhir (3) </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="Your answer" name="nama_penguji_tugas_akhir3" value="{{ $wisu->nama_penguji_tugas_akhir3 }}" >
                  </td>
                </tr>


                <tr>
                <td>
                    <p>Upload Scan Asli Kartu Keluaga (file PDF)</p>
                    <p>Upload Scan Asli Akte Kelahiran (File PDF) *</p>
                    <p>Upload Foto Berwarna Background Putih</p>
                    <p>Upload Foto Berwarna Background Merah *</p>
                    <p>Upload Ijazah SMA/SMK/MA/Paket C </p>
                    <p>Upload Scan KTP Asli</p>
                    <p>Upload Scan Bukti Pembayaran Biaya Wisuda</p>
                    <p>Upload Scan Bukti Penyerahan KTM</p>
                    <p>Upload Scan Lembar Pengesahan Tugas Akhir </p>
                    <p>Upload Scan Sertifikat PPK (Program Pengenalan Kampus)</p>
                    <p>Upload Scan Bukti Penyerahan Laporan PKL I, II, III dan Proyek Akhir</p>
                    <p>Upload Scan Bukti Bebas Tanggungan Pustaka (Politeknik LPP)</p>
                    <p>Upload Scan Bukti Bebas Tanggungan Administrasi (Bagian Keuangan)</p>
                    <p>Upload Scan Surat Keterangan mengikuti Kegiatan Wajib Non-Akademik Bela Diri</p>
                    <input type="file" name="filekk[]" multiple="true" data-min-file-count="2">harus lebih dari 2 filu untuk mengirim file
                  </td>
                </tr>
                <?php $ns++ ?>
                <tr>
                  <td>
                    <input type="submit" value="Kirim">
                  </td>
                </tr>
              </form>
          @endif
          @endif
          @endforeach
          @endforeach
          
        </table>
      </div>
    </div>
  </div>

  @endif
  @endforeach
  @endsection

  @section('footer')
  <!-- Footer -->
  <footer class="w3-container w3-padding-16 w3-light-grey">
    <center><p>Copyright © 1994 - 2020 LPP YOGYAKARTA. </p></center>
  </footer>
@endsection
