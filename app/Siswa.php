<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $fillable = [
         'id', 'user_id', 'nama_depan', 'nama_belakang', 'email', 'status', 'foto'
    ];

}
