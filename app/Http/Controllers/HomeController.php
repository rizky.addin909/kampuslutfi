<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Siswa;
use App\Wisuda;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $users = User::get();
        $siswas = Siswa::where('user_id', auth()->user()->id)->get();
        $sis= 'siswa';
        $jumser = User::where('status', $sis)->get()->take(1);
        $wisudas = Wisuda::get()->take(1);
        return view('admin/landing', compact('users', 'jumser', 'siswas', 'wisudas'));
    }
}
