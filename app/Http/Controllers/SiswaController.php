<?php

namespace App\Http\Controllers;

use App\Siswa;
use Illuminate\Http\Request;

use Image;
use Auth;
use App\User;
use App\Wisuda;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $siswa = Siswa::get();
        if( $user == null ){
            abort(403,'Anda tidak punya akses ke halaman ini silahkan login');
        }else{
            $users = User::where('id', auth()->user()->id)->get();
            $siswas = Siswa::where('user_id', auth()->user()->id)->get();
            return view('siswa.SiswaLanding', compact('users', 'user', 'siswa', 'siswas' ));
        }
    }

    public function daftarwisuda()
    {
        $user = Auth::user();
        $siswa = Siswa::get();
        $wisud = Wisuda::get();
        if( $user == null ){
            abort(403,'Anda tidak punya akses ke halaman ini silahkan login');
        }else{
            $users = User::where('id', auth()->user()->id)->get();
            $siswas = Siswa::where('user_id', auth()->user()->id)->get();
            $siswad = Siswa::where('user_id', auth()->user()->id)->get('id')->first();
            $wisuds = Wisuda::where('id_siswa', $siswad['id'])->get();
            return view('siswa.SiswaWisuda', compact('users', 'user', 'siswa', 'siswas', 'wisud', 'wisuds' ));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user()->id;
        $siswa = Siswa::where('user_id', $user)->get('user_id')->first();
        
        if(  $siswa['user_id'] != $user ){
            $siswa = siswa::create([
                'user_id' => Auth::user()->id,
                'nama_depan' => Auth::user()->name,
                'email' => Auth::user()->email,
		'status' => 'Proses'
            ]);
            return redirect()->route('siswa');
        }else{
            return redirect()->route('siswa');
        }
    }

    public function storewisuda(Request $request)
    {
        
        $wisud = Wisuda::where('id_siswa', $request->sisid)->get('id_siswa')->first();
            if($request->hasFile('filekk')){
                $files= $request->file('filekk');
                $NameArr = [];
                foreach ($request->filekk as $file) {
                    $filename = $file->getClientOriginalName();
                    $NameArr[] = $filename;
                    $file->move('uploads/wisudas/' , $filename);
                }
                if (count($NameArr) == null) {
                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_kk' => ' ',
                        'file_akte' => ' ',
                        'file_bbp' => ' ', 
                        'file_bbm' => ' ', 
                        'file_ijazah' => ' ', 
                        'file_ktp' => ' ', 
                        'file_bpbw' => ' ', 
                        'file_bpktm' => ' ', 
                        'file_lpta' => ' ', 
                        'file_sppk' => ' ', 
                        'file_bplpkl' => ' ', 
                        'file_bbtp' => ' ', 
                        'file_bbta' => ' ', 
                        'file_beladiri' => ' ',
                    ]);
                }
                if (count($NameArr) == 2) {
                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_kk' => $NameArr[0],
                        'file_akte' => $NameArr[1],
                    ]);
                }
                if (count($NameArr) == 3) {
                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_kk' => $NameArr[0],
                        'file_akte' => $NameArr[1],
                        'file_bbp' => $NameArr[2],
                    ]);
                }
                if (count($NameArr) == 4) {
                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_kk' => $NameArr[0],
                        'file_akte' => $NameArr[1],
                        'file_bbp' => $NameArr[2],
                        'file_bbm' => $NameArr[3], 
                    ]);
                }
                if (count($NameArr) == 5) {
                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_kk' => $NameArr[0],
                        'file_akte' => $NameArr[1],
                        'file_bbp' => $NameArr[2],
                        'file_bbm' => $NameArr[3],
                        'file_ktp' => $NameArr[5],  
                    ]);
                }


                
                
            }
            
            Wisuda::where('id_siswa', $request->sisid)->update([
                    'email' => $request->email,
                    'nama_lengkap' => $request->namlep,
                    'nim' => $request->nim,
                    'alamat_email' => $request->alemail,
                    'nohp' => $request->nohp,
                    'nama_ortu' => $request->namortu,
                    'prodi' => $request->prodi,
                    'tempat_lahir' => $request->temla,
                    'tanggal_lahir' => $request->tangla,
                    'alamat_domisili' => $request->alamat,
                    'nik' => $request->nik,
                    'tempat_pkl1' => $request->tempat_pkl1, 
                    'tanggal_pelaksanaan' => $request->tanggal_pelaksanaan , 
                    'judul_pkl1' => $request->judul_pkl1, 
                    'tempat_pkl2' => $request->tempat_pkl2, 
                    'tanggal_pelaksanaan2' => $request->tanggal_pelaksanaan2, 
                    'judul_pkl2' => $request->judul_pkl2, 
                    'tempat_pkl3' => $request->tempat_pkl3, 
                    'tanggal_pelaksanaan3' => $request->tanggal_pelaksanaan3, 
                    'judul_pkl3' => $request->judul_pkl3, 
                    'judul_tugas_akhir' => $request->judul_tugas_akhir, 
                    'nama_pembimbing_tugas_akhir1' => $request->nama_pembimbing_tugas_akhir1, 
                    'nama_pembimbing_tugas_akhir2' => $request->nama_pembimbing_tugas_akhir2, 
                    'nama_pembimbing_tugas_akhir3' => $request->nama_pembimbing_tugas_akhir3, 
                    'nama_penguji_tugas_akhir1' => $request->nama_penguji_tugas_akhir1, 
                    'nama_penguji_tugas_akhir2' => $request->nama_penguji_tugas_akhir2, 
                    'nama_penguji_tugas_akhir3' => $request->nama_penguji_tugas_akhir3,
                ]);
            return redirect()->route('daftarwisuda');;
    }

    public function storewisuda1(Request $request, $id)
    {
        
        $wisud = Wisuda::where('id_siswa', $id)->get('id_siswa')->first();

        if(  $wisud['id_siswa'] != $id ){
            $wisud = wisuda::create([
                'id_siswa' => $id,
                'email' => Auth::user()->email
            ]);
            return redirect()->route('daftarwisuda');
        }else{
            return redirect()->route('daftarwisuda');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function show(Siswa $siswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function edit(Siswa $siswa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Siswa $siswa)
    {
        Siswa::where('user_id', auth()->user()->id)->update([
            'nama_depan' => $request->namadep,
            'nama_belakang' => $request->namabel,
            'email' => $request->email,
            'telepon' => $request->notelp,
            'alamat' => $request->alamat,
        ]);

        User::where('id', auth()->user()->id)->update([
            'name' => $request->namadep,
        ]);
        return redirect()->route('siswa')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Siswa $siswa)
    {
        //
    }

    public function update_avatar(Request $request)
    {
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(500, 500)->save( public_path('/uploads/avatars/' . $filename) );

            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }
        $users = User::where('id', auth()->user()->id)->get();
        $siswa = Siswa::get();
        $siswas = Siswa::where('user_id', auth()->user()->id)->get();

        return view('siswa.SiswaLanding', compact('users', 'siswa', 'siswas'), array('user' => Auth::user()));

    }
}
