<?php

namespace App\Http\Controllers;

use App\Wisuda;
use Illuminate\Http\Request;

class WisudaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Wisuda  $wisuda
     * @return \Illuminate\Http\Response
     */
    public function show(Wisuda $wisuda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Wisuda  $wisuda
     * @return \Illuminate\Http\Response
     */
    public function edit(Wisuda $wisuda)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Wisuda  $wisuda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wisuda $wisuda)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wisuda  $wisuda
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wisuda $wisuda)
    {
        //
    }
}
