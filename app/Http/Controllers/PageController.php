<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Siswa;
use App\Wisuda;
use App\Berita;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = Berita::get();

        return view('home/landing', compact('berita'));
    }
    public function index2()
    {
        return view('home/landing2');
    }

    public function userview()
    {
        $users = User::get();

        $siswas = Siswa::get();
        return view('admin/UserRecord', compact('users', 'siswas'));
    }

    public function wisudaview()
    {
        $users = User::get();

        $siswas = Siswa::get();
        $wisuda = Wisuda::get();
        return view('admin/WisudaRecord', compact('users', 'siswas', 'wisuda'));
    }

    public function beritaview()
    {
        $users = User::get();

        $siswas = Siswa::get();
        $berita = Berita::get();
        $nil = 1;
        return view('admin/BeritaRecord', compact('users', 'siswas', 'berita', 'nil'));
    }

    public function tambahberitaview()
    {
        $users = User::get();

        $siswas = Siswa::get();
        return view('admin/TambahBerita', compact('users', 'siswas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function edituser(Request $request, $id)
    {
        $userid = $request->iduser;
        $users = User::where('id', $userid)->get();
        $siswas = Siswa::where('user_id', $userid)->get();
        return view('admin/EditUser', compact('users', 'id', 'siswas'));
    }

    public function editwisuda(Request $request, $id)
    {
        $wisudaid = $request->idsiswa;
        $wisuda = Wisuda::where('id_siswa', $wisudaid)->get();
        $siswas = Siswa::where('id', $wisudaid)->get();
        $users = User::get();
        return view('admin/EditWisuda', compact('wisuda', 'id', 'siswas', 'users'));
    }

    public function editberita(Request $request, $id)
    {
        $userid = $request->idberita;
        $users = User::where('id', $userid)->get();
        $siswas = Siswa::where('user_id', $userid)->get();
        $berita = berita::where('id', $userid)->get();
        return view('admin/EditBerita', compact('users', 'id', 'berita', 'siswas'));
    }

    public function tambahberita(Request $request)
    {
        $berita = new Berita;
        $berita->user_id = auth()->user()->id;
        $berita->berita = $request->berita;
        $berita->save();
        return redirect()->route('viewberita')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $User)
    {
        $userid = $request->iduser;
        User::where('id', $userid)->update([
            'name' => $request->username,
            'email' => $request->email,
            'status' => $request->status,
        ]);

        Siswa::where('user_id', $userid)->update([
            'status' => $request->stat,
        ]);
        return redirect()->route('viewuser')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    public function updatewisuda(Request $request, User $User)
    {
        $userid = $request->sisid;
        Siswa::where('id', $userid)->update([
            'status' => $request->stat,
        ]);
        return redirect()->route('viewwisuda')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    public function updateberita(Request $request, User $User)
    {
        $userid = $request->berid;
        Berita::where('id', $userid)->update([
            'berita' => $request->berita,
        ]);
        return redirect()->route('viewberita')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function deleteberita($id)
    {
        Berita::where('id',$id)->delete();
        return redirect()->route('viewberita')->with('success_message', 'Deskripsi berhasil dihapus');
    }

    public function destroy($id)
    {
        //
    }
}
